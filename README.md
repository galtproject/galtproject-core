# Galt Project Core Contracts (@galtproject/core)

[![Build Status](https://travis-ci.org/galtproject/galtproject-core.svg?branch=develop)](https://travis-ci.org/galtproject/galtproject-core)
[![Contracts Version](https://img.shields.io/badge/version-0.12.0-orange.svg)](https://github.com/galtproject/galtproject-core)
[![Telegram Chat](https://img.shields.io/badge/telegram-chat-blue.svg)](https://t.me/galtproject)


### About Galt Project
The main goal of Galt Project is to create sofware for land tokenization (accounting, buying and selling on blockchain) and self-governance. And to create a communities, runned by a smart contracts with the help of this software.

https://galtproject.io/

### Other Galt Project Modules

* [@galtproject/galt-token](https://github.com/galtproject/galtproject-galt-token)
* [@galtproject/galt-distribution](https://github.com/galtproject/galtproject-galt-distribution)
* [@galtproject/libs](https://github.com/galtproject/galtproject-libs)
* [@galtproject/geodesic](https://github.com/galtproject/galtproject-geodesic)
* [@galtproject/math](https://github.com/galtproject/galtproject-math)
* [@galtproject/market](https://github.com/galtproject/galtproject-market)
* [@galtproject/token-fund-basic](https://github.com/galtproject/galtproject-fund-basic)

### About Copyright and Software licenses
The main purpose of Galt Protocol is to create open and reliable instrument for self-government communities of Property Owners. According to that, one of our main goals in Galt Project is to create pure community driven and open software product. For now Galt Project if fully self-funded, we have certain vision and we want to implement it. To ensure the achievement of our vision, before the launch, protocol is open-sourced, but it is and it will be our intellectual property. You need to ask us, if you want to participate or use our code. We will decide if it's possible at that moment.

After the first version of protocol will be released in Ethereum Mainnet and initial Galt Auction will end, we will release all software (including front-end) under one of public licences. But before that, please, respect the time and work of others and the will of the creators!

